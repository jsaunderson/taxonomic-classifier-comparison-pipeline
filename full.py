from __future__ import print_function
from scipy.stats import spearmanr
import argparse, os, subprocess, queue, threading, glob, re, time

#Arguments to parse for execution
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--download", help="file of links to download")
parser.add_argument("-s", "--storage", default="fna", help="directory for fna files")
parser.add_argument("-a", "--abundance", default="abd", help="directory for abundance tables")
parser.add_argument("-g", "--generateabd", type=int, nargs="?", const=10, help="generate abundance tables [num stag tables]")
parser.add_argument("-c", "--cores", type=int, default=1, help="number of cores to utilize")
parser.add_argument("-o", "--output", default="out", help="directory for output fastq files")
parser.add_argument("-r", "--readdepths", help="file containing read depths")
parser.add_argument("-m", "--model", default="/home/joe/apps/GemSIM/models/ill100v5_s.gzip", help="path to GemSIM model")
parser.add_argument("-k", "--krakendb", default="/APPS/kraken/minikraken_20141208/", help="path to Kraken DB")
parser.add_argument("-b", "--bowtiedb", default="/APPS/metaphlan2/db_v20/mpa_v20_m200", help="path to BowTie DB")
parser.add_argument("--results", default="results", help="directory for resulting graphs")
parser.add_argument("--conservative", action="store_true", help="throw away files when done")
parser.add_argument("--metakrak", default="krakenmarkers", help="kraken meta information file prefix")
parser.add_argument("--metameta", default="metamarkers", help="metaphlan meta information file prefix")
parser.add_argument("--skipsim", action="store_true", help="skip generation of simulated fastq")
parser.add_argument("--skipprofile", action="store_true", help="skip profiling simulated fastqs")
parser.add_argument("--skipcalc", action="store_true", help="skip calculations and graphing")
parser.add_argument("--profile", action="store_true", help="profile resource usage")
parser.add_argument("--fine", action="store_true", help="profile resource usage more finely")

args = parser.parse_args()

#Spearman object for providing spearman correlation calculation
class SpearmanCalc:
    def __init__(self):
        self.data = []

    def reset_items(self):
        self.data = []

    def add_item(self, item):
        self.data.append(item)

    def calc(self):
        # print(self.data)
        return spearmanr(self.data)

#TODO: Changed hardcoded download specifically for ftp directory
#Given the name of a text file will download the listed files automatically,
#any *.gz files will be unzipped
def download_files(txt):
    print(">Attempting to download all files listed in " + txt)
    import urllib2, ftplib
    f = open(txt, 'r')
    ftp = ftplib.FTP("ftp.ncbi.nih.gov")
    ftp.login()

    if not os.path.exists(args.storage):
        print("Created directory " + args.storage)
        os.makedirs(args.storage)

    dlcount = 0
    for x in f:
        dlcount += 1
        x = x.rstrip('\n')
        if x.startswith("http"):
            down = urllib2.urlopen(x)
            with open(args.storage + "/" + os.path.basename(x), "wb") as save:
                save.write(down.read())
            print("Downloaded " + os.path.basename(x) + " from " + x)
        else:
            ftp.cwd(x[22:])
            files = ftp.nlst()
            if "latest_assembly_versions" in files:
                ftp.cwd("latest_assembly_versions")
            elif "all_assembly_versions" in files:
                ftp.cwd("all_assembly_versions/suppressed/")
            ftp.cwd(ftp.nlst()[-1])
            filesx = ftp.nlst()
            for q in filesx:
                if q.endswith("genomic.fna.gz") and "_from_" not in q:
                    lf = open(args.storage + "/" + q,   "wb")
                    ftp.retrbinary("RETR " + q, lf.write)
                    lf.close()
                    print("Downloaded " + q + " via ftp")
    print("Unzipping any gzip'd files")
    subprocess.call("gunzip " + args.storage + "/*.gz", shell=True)
    print("Download file contained " + str(dlcount) + " links; directory now contains " + str(len(os.listdir(args.storage))) + " files")

def match_source_files(filenameX):
    files = os.listdir(args.storage)

    #grep -P -o "(?<='taxon': ')[^']+(?=')" markers_info.txt | sort -u > metamarkers.txt
    with open(filenameX, "r") as f:
        allG = f.readlines()

    #Change k__ to d__ for consistency between metaphlan/kraken
    allG = [re.sub('k__', 'd__', l.lower()) for l in allG]

    # processed = [x for x in allG if "t__" not in x and "s__" not in x and "g__" in x]

    #print len(processed)
    patternStrip = re.compile("(?:>[^ ]* )(([A-Za-z]*[a-z][A-Za-z]* )*)")
    patternStrim = re.compile("()")
    fullDict = {}

    classes = ['f', 'p', 'c', 'o', 'g']
    #classes = ['g']
    allDict = {
        "f": {},
        "p": {},
        "c": {},
        "o": {},
        "g": {},
        "s": {}
    }

    #Hardcoded automatic processing, change for dynamic name pickup
    autoproc = [2,1,2,1,2,1,1,2]
    autoK = 0

    setDict = {
        "f": {},
        "p": {},
        "c": {},
        "o": {},
        "g": {},
        "s": {}
    }
    for line in allG:
        for k in line.split()[0].split("|"):
            if k[0] in classes:
                if k[3:] not in setDict[k[0]]:
                    setDict[k[0]][k[3:]] = 1
                else:
                    setDict[k[0]][k[3:]] += 1

    print(len(setDict["f"]))
    print(len(setDict["p"]))
    print(len(setDict["c"]))
    print(len(setDict["o"]))
    print(len(setDict["g"]))
    print(len(setDict["s"]))



    toProc = []
    for x in files:
        tempProc = []
        with open(args.storage + '/' + x, "r") as f:
            # print f.readline()
            rn = patternStrip.match(re.sub('[\[\]]|strain|genome assembly', '', f.readline())).group(1).lower()
            print(rn)
            for line in allG:
                if line.startswith("d__viruses") or not line.startswith("d"):
                    continue
                found = True
                for part in rn.split(" "):
                    if part not in line:
                        found = False
                        break
                if found:
                    #tempProc.append(line.strip().split()[0].split("|s__")[0])
                    tempProc.append(line.strip().split()[0].split("_cag:")[0].split("|t__")[0])
                    # for fPart in line.strip().split()[0].split("|"):
                    #     if fPart[0] in classes:
                    #         allDict[fPart[0]].append(fPart[3:])
        tempProc = sorted(set(tempProc))
        if len(tempProc) >= 1:
            tempTempProc = []
            for kkko in tempProc:
                itsgood = True
                for kkkoo in rn.split():
                    if '_' + kkkoo not in kkko:
                        itsgood = False
                if itsgood:
                    tempTempProc.append(kkko)
    #            print tempTempProc
            if len(tempTempProc) >= 1:
                tempProc = tempTempProc
                tempTempProc = []
                for kkko in tempProc:
                    if not(any(ixx.isdigit() for ixx in kkko) or '[' in kkko):
                        tempTempProc.append(kkko)
    #            print tempTempProc
                if len(tempTempProc) >= 1:
                    tempProc = tempTempProc
            if len(tempProc) > 1:
                print("Multiple matches, using first item!")
            toProc += [tempProc[0]]
            print ("MATCH " + tempProc[0])

    #MANUAL SELECTION
    #    if len(tempProc) > 1:
    #        chosen = False
    #        for k in range(len(tempProc)):
    #            print "%d) %s" % (k+1, tempProc[k])
    #        while chosen is False:
    #            try:
    #                ans = int(raw_input("Enter desired map: "))-1
    #                if ans in range(len(tempProc)):
    #                    chosen = ans
    #                else:
    #                    print "Entered %d, Answer must between 1 and %d" % (ans, len(tempProc))
    #            except ValueError:
    #                print "Input could not be converted to integer"
    #        # chosen = autoproc[autoK]-1
    #        autoK += 1
    #        tempProc = [tempProc[chosen]]
        elif len(tempProc) == 0:
            print( "---FAILED TO MATCH---")
            toProc += ["null"]

    #toProc = sorted(toProc)
    print( "\n".join(toProc))
    print (len(toProc))

    #for part in line add to relevant dictionary
    #make into key + count (dictionary) -- write out to seperate files
    # xd
    xk = 0
    with open("%s_meta_s" % (filenameX.split(".")[0]), 'w') as f:
        for line in toProc:
            for fPart in line.split("|"):
                if fPart[0] in classes:
                    f.write("%s\t%s\n" % (fPart[3:], files[xk]))
                    if fPart[3:] not in allDict[fPart[0]]:
                        allDict[fPart[0]][fPart[3:]] = 1
                    else:
                        allDict[fPart[0]][fPart[3:]] += 1
            xk += 1
    for c in classes:
        with open("%s_meta_%s" % (filenameX.split(".")[0], c), 'w') as f:
            f.write("%s\t%d\n" % ("total", len(setDict[c])))
            for name, matches in allDict[c].iteritems():
                f.write("%s\t%d\n" % (name, matches))


gAbdCount = 0
#Writes an 'abundance table' to file, corrects any errors caused by the
#distribution being inexact
def abd_correct_output(x, y, genType, repeats):
    import random
    global gAbdCount
    correction_index = 0
    while sum(x) != 10000000:
        correction_index = (correction_index + 1) % len(x)
        if sum(x) > 10000000:
            x[correction_index] -= 1
        else:
            x[correction_index] += 1
    for q in range(0, repeats):
        random.shuffle(y)
        fileName = args.abundance + '/abd_' + str(gAbdCount) + '_' + genType + '.txt'
        with open(fileName, 'w') as f:
            for i, z in enumerate(x):
                f.write("%s\t0.%07d\n" % (y[i],z))
        print("Generated " + genType + " abundance table as " + fileName)
        gAbdCount += 1

#Contains formula used to generate a staggered distribution
def stag_gen(num):
    signif = 0.6;
    x = 3;
    nums = []

    while(num > 0):
        nums.append(int(signif*10000000))
        x -= .4
        x = x if x > 1.3 else 1.3
        signif /= x
        num -= 1
    error = 10000000 - sum(nums)
    if error > 0:
        nums[0] -= error
    else:
        nums[0] += error
    return nums

#Generates a staggered and even abundance table
def abd_gen():
    print(">Creating abundance tables")
    if not os.path.exists(args.abundance):
        print("Created directory " + args.abundance)
        os.makedirs(args.abundance)

    files = os.listdir(args.storage)
    fCount = len(files)
    print("Found %d samples to generate abundance tables with in %s" % (fCount, args.storage))
    print("Generating an even abundance table")
    abd_correct_output([10000000/fCount] * fCount, files, 'even', 1)
    print("Generating " + str(args.generateabd) + " staggered abundance tables")
    abd_correct_output(stag_gen(fCount), files, 'stag', args.generateabd)

#Print lock ensures only one thread will print at a time
print_lock = threading.Lock()

#Worker used for multithreading GemSIM
def worker(threadN):
    logf = open(args.output + "/log" +  str(threadN) + ".txt", 'w', 1)
    while True:
        params = q.get()
        if params is None:
            with print_lock:
                print("Queue empty, stopping thread %d" % threadN)
            logf.close()
            return
        with print_lock:
            print("%d items remaining to process" % (q.qsize()-int(args.cores)))
            print("Starting GemSIM with read depth: %s, abundance table: %s on thread %d" % (params[0], params[1], threadN))
        outfile = args.output + "/r" + params[0] + "_" + params[1][4:-4]
        logf.write(("----NEW GEMSIM RUN----\nRead Depth: %s\nAbundance Table: %s\nOutput File: %s\n" % (params[0], params[1], outfile)))
        rcode = subprocess.call(["GemReads.py", "-R", args.storage, "-a",
        args.abundance + "/" + params[1], "-n", params[0], "-l", "100", "-m",
        args.model, "-q", "64", "-o", outfile], stdout=logf, stderr=logf)
        #TODO:ADD GZIP CODE IN HERE
        with print_lock:
            print("GemSIM terminated on thread %d with return code %d for output file %s" % (threadN, rcode, outfile))

#Active_cores maintains current core count, lock_cores is a lock for reading or
#changing active_cores
lock_cores = threading.Lock()
active_cores = int(args.cores)

#Worker used for multithreading MetaPhlAn
def worker2(threadN, coresN, targetCores):
    logf = open(args.output + "/proflog" + str(threadN) + ".txt", 'w', 1)
    global mfiles, active_cores, print_lock
    while True:
        #TODO: Change hardcoded 35; modify how we process read nums to generate
        if targetCores == -1 and q.qsize() <= 35:
            with print_lock:
                print('Stopping thread %d to allow core reallocation' % threadN)
            logf.close()
            with lock_cores:
                active_cores -= coresN
            return
        contents = q.get()
        if contents is None:
            with print_lock:
                print("Retrieved 'None' from Queue, stopping thread %d" % threadN)
            with lock_cores:
                active_cores -= coresN
            logf.close()
            return
        targetfile = contents[1]
        #For large file sizes, block until the target number of cores has been
        #reached
        if contents[0]:
            while coresN != targetCores:
                with lock_cores:
                    core_dif = min(int(args.cores) - active_cores, targetCores - coresN)
                    if core_dif > 0:
                        active_cores += core_dif
                        coresN += core_dif
                        with print_lock:
                            print("Thread %d gained %d core(s) and now has %d cores" % (threadN, core_dif, coresN))
                if coresN != targetCores:
                    time.sleep(1)
        with print_lock:
            print("%d item(s) remaining in queue to be processed" % (q.qsize()-int(args.cores)))
            print("Running MetaPhlAn on %s on thread %d with %d cores" % (targetfile, threadN, coresN))
        rcode = subprocess.call(["metaphlan2.py", targetfile, "-o", targetfile + ".mout", "--nproc", str(coresN), "--bowtie2db", args.bowtiedb, "--mpa_pkl", args.bowtiedb + ".pkl", "--input_type", "fastq"], stdout=logf, stderr=logf)
        with print_lock:
            print("MetaPhlAn terminated on thread %d with return code %d for input file %s" % (threadN, rcode, targetfile))
        if rcode == 0:
            mfiles.append(targetfile + ".mout")

#Worker used for multithreading Kraken
def worker4(threadN, coresN, targetCores):
    logf = open(args.output + "/proflog" + str(threadN) + ".txt", 'w', 1)
    global kfiles, active_cores, print_lock
    while True:
        #TODO: Change hardcoded 35; modify how we process read nums to generate
        if targetCores == -1 and q.qsize() <= 35:
            with print_lock:
                print('Stopping thread %d to allow core reallocation' % threadN)
            logf.close()
            with lock_cores:
                active_cores -= coresN
            return
        contents = q.get()
        if contents is None:
            with print_lock:
                print("Retrieved 'None' from Queue, stopping thread %d" % threadN)
            with lock_cores:
                active_cores -= coresN
            logf.close()
            return
        targetfile = contents[1]
        #For large file sizes, block until the target number of cores has been
        #reached
        if contents[0]:
            while coresN != targetCores:
                with lock_cores:
                    core_dif = min(int(args.cores) - active_cores, targetCores - coresN)
                    if core_dif > 0:
                        active_cores += core_dif
                        coresN += core_dif
                        with print_lock:
                            print("Thread %d gained %d core(s) and now has %d cores" % (threadN, core_dif, coresN))
                if coresN != targetCores:
                    time.sleep(1)
        with print_lock:
            print("%d item(s) remaining in queue to be processed" % (q.qsize()-int(args.cores)))
            print("Running Kraken on %s on thread %d with %d cores" % (targetfile, threadN, coresN))
        baseF = args.output + "/" + os.path.basename(targetfile)
        rcode = subprocess.call(["kraken", "--db", args.krakendb, "--threads", str(coresN), "--output",  baseF + ".kout", targetfile, "--preload"], stdout=logf, stderr=logf)
        with print_lock:
            print("Kraken terminated on thread %d with return code %d for input file %s" % (threadN, rcode, targetfile))
        if rcode == 0:
            kfiles.append(baseF + ".kout")


#Simulate reads utilizing GemSIM and the worker thread
def threaded_sim():
    print("Running GemSIM on %d thread(s)" % args.cores)
    if not os.path.exists(args.output):
        print("Created directory " + args.output)
        os.makedirs(args.output)

    if args.readdepths:
        print("Reading read depths from file %s" % args.readdepths)
        sizes = []
        with open(args.readdepths, 'r') as f:
            for line in f:
                line = line.strip()
                if line.isdigit():
                    sizes.append(line)
    else:
        print("Using default read depths")
        sizes = ["1000", "10000", "100000"]
    sizes.sort()
    print("Using %s as read depths" % ', '.join(sizes))

    global q
    q = queue.Queue()

    files = os.listdir(args.abundance)
    print("Using %d files from %s as abundance tables" % (len(files), args.abundance))
    for size in sizes[::-1]:
        for fx in files:
            q.put((size, fx))

    threads = [ threading.Thread(target=worker, args=[_i] ) for _i in range(args.cores) ]
    for thread in threads:
        thread.start()
        q.put(None)

    for thread in threads:
        thread.join()

    print("All threads have terminated, %d files in output directory" % len(os.listdir(args.output)))
#    print("Exiting %s..." % parser.prog)

#Run taxonomic classifiers on the generated files
def profile():
    print(">Profiling fastq's")
    logf = open(args.output + "/profilelog.txt", 'w', 1)
    files = glob.glob(args.output + '/*.fastq')
    # print files
    files.sort(key=lambda f: (f.split('/')[-1].split('_')[0], int(f.split('/')[-1].split('_')[1])))

    global kfiles
    kfiles = [];
    global mfiles
    mfiles = [];

    global q
    q = queue.Queue()

    for fileX in files:
        baseF = args.output + "/" + os.path.basename(fileX)
        #TODO: change hardcoded boolean blocking switch below
        if "r10000000" in baseF:
            q.put((True, baseF))
        else:
            q.put((False, baseF))

    thread_no = 11
    corelist = [args.cores / thread_no] * thread_no
    targcore = 0
    while sum(corelist) != args.cores:
        if sum(corelist) < args.cores:
            corelist[targcore] += 1
        else:
            corelist[targcore] -= 1
        targcore += 1
    while len(corelist) != args.cores:
        corelist.append(-1)

    print("RUNNING KRAKEN")

    threads = [ threading.Thread(target=worker4, args=[_i, 1, corelist[_i]] ) for _i in range(len(corelist)) ]
    for thread in threads:
        thread.start()
        q.put(None)

    for thread in threads:
        thread.join()

    print("RUNNING METAPHLAN")

    q = queue.Queue()

    for fileX in files:
        baseF = args.output + "/" + os.path.basename(fileX)
        #TODO: change hardcoded boolean blocking switch below
        if "r10000000" in baseF:
            q.put((True, baseF))
        else:
            q.put((False, baseF))

    threads = [ threading.Thread(target=worker2, args=[_i, 1, corelist[_i]] ) for _i in range(len(corelist)) ]
    for thread in threads:
        thread.start()
        q.put(None)

    for thread in threads:
        thread.join()

    print("All threads have terminated, continuing - %d files in mFiles" % len(mfiles))

    print("Creating Kraken Report")
    print("Running kraken-report with %d .kout files" % len(kfiles))
    koutfile = open(args.output + "/krakenrep.txt", "w", 1)
    koutfile.write("ID\t" + "\t".join(kfiles) + "\n#SampleID" + "\tKraken_Analysis" * len(kfiles) + "\n")
    rcode = subprocess.call(["kraken-mpa-report", "--db", args.krakendb] + kfiles, stdout=koutfile, stderr=logf)
    print("Finished kraken-report with return code %d" % rcode)

    print("Creating MetaPhlAn Report")
    print("Running merge_metaphlan_tables.py with %d .mout files" % len(mfiles))
    moutfile = open(args.output + "/metaphlanrep.txt", "w")
    rcode = subprocess.call(["merge_metaphlan_tables.py"] + mfiles, stdout=moutfile, stderr=logf)
    print("Finished merge_metaphlan_tables with return code %d" % rcode)

    koutfile.close()
    moutfile.close()
    logf.close()

#Correlate input/output data to generate performance statistics
def calc():
    print(">Mapping data, calculating sensitivity/specificity and graphing roc curve")
    if not os.path.exists(args.results):
        print("Created directory " + args.results)
        os.makedirs(args.results)
    if not os.path.exists('extras'):
        print("Created directory extras")
        os.makedirs('extras')

    classes = ['f', 'p', 'c', 'o', 'g']
    allMeta = {
        'm': {
            key: {} for key in classes
        },
        'mTotal': {},
        'k': {
            key: {} for key in classes
        },
        'kTotal': {}
    }

    files = os.listdir(args.storage)
    fnaHeads = {}
    for fileX in files:
        with open(args.storage + "/" + fileX) as f:
            fnaHeads[fileX] = f.readline().lower().rstrip()


    threshold = 1

    #TODO: CHANGE HARDCODED 56 MAX

    with open("extras/verbose.csv", 'w') as extraf:
        extraf.write("%s,%s,%s,%s,%s,%s\n" % ("program", "abd_source", "read_depth", "tax_level", "pres", "abs"))

    for p in ['m', 'k']:
        print("Reading %s meta information from %s_meta_<*classes>" % (('Kraken', args.metakrak) if p == 'k' else ('MetaPhlAn', args.metameta)))
        for classType in classes:
            with open((args.metakrak if p == 'k' else args.metameta) + "_meta_" + classType, "r") as f:
                allMeta[p + 'Total'][classType] = int(f.readline().split()[1])
                for line in f:
                    parts = line.split()
                    allMeta[p][classType][parts[0]] = int(parts[1])

        endresults = {
            key: [] for key in classes
        }
        for i in range(56):
            for key in classes:
                endresults[key].append({})

        with open(args.output + "/" + ("metaphlanrep.txt" if p == 'm' else "krakenrep.txt")) as f:
            head = f.readline().split()
            for i in range(1, len(head)):
                head[i] = (head[i].split("/")[-1].split("_")[0][1:], "abd_" + "_".join(head[i].split("_")[1:3]))
                # print head[i]
            #discard the metadata line
            f.readline()
            for r in f:
                r = r.lower().split()
                #TODO: More advanced filter than species level only
                if "s__" in r[0]:
                    for k in range(1, len(r)):
                        if (float(r[k])*int(head[k][0]) if float(r[k]) < 1 else float(r[k])) > threshold:
                            for classType in classes:
                                realName = r[0].split(classType + "__")[-1].split("|")[0]
                                if realName in endresults[classType][k]:
                                    endresults[classType][k][realName] += 1
                                else:
                                    endresults[classType][k][realName] = 1
        print("Calculating statistics and writing to %sresults.csv" % p)
        with open(args.results + "/" + p + "results.csv", 'w') as f:
            f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % ("abd_source", "read_depth", "tax_level", "tpr", "tnr", "trupos", "falsepos", "falseneg", "trueneg", "spear.val", "spear.p"))
            spearcalc =  SpearmanCalc()
            for classType in classes:
                for k in range(1, len(endresults[classType])):
                    #DIRTY FIX FOR OUT OF RANGE
                    if k >= len(head):
                        print("OUT OF RANGE -> breaking")
                        break
                    truepos = 0
                    falsepos = 0
                    spearcalc.reset_items()
                    for item in endresults[classType][k]:
                        if item in allMeta[p][classType]:
                            truepos += 1
                            spearcalc.add_item([endresults[classType][k][item], allMeta[p][classType][item]])
                        else:
                            falsepos += 1
                            spearcalc.add_item([endresults[classType][k][item], 0])
                    for item in allMeta[p][classType]:
                        if item not in endresults[classType][k]:
                            spearcalc.add_item([0, allMeta[p][classType][item]])

                    #Change hardcoded 50
                    # if falsepos == 0 and truepos == 0:
                    #     continue
                    spearval = spearcalc.calc()
                    falseneg =  len(allMeta[p][classType]) - truepos
                    trueneg = allMeta[p + 'Total'][classType] - truepos - falsepos - falseneg
                    # print falseneg, trueneg, truepos, falsepos, classType
                    # print allMeta['mTotal']
                    # tempfix
                    if (trueneg + falsepos) == 0:
                        tnr = float('inf')
                    else:
                        tnr = float(trueneg)/(trueneg + falsepos)
                    if (truepos + falseneg) == 0:
                        tpr = float('inf')
                    else:
                        tpr = float(truepos)/(truepos + falseneg)
                    with open("extras/verbose.csv", 'a') as extraf:
                        for xline in spearcalc.data:
                            # print(xline)
                            extraf.write("%s,%s,%s,%s,%d,%d\n" % (p, head[k][1], head[k][0], classType, xline[0], xline[1]))
                    f.write("%s,%s,%s,%s,%s,%d,%d,%d,%d,%s,%s\n" %(head[k][1], head[k][0], classType, tpr, tnr, truepos, falsepos, falseneg, trueneg, spearval[0], spearval[1]))


    # Number of metaphlan species
    #metanum = 7678
    # Number of kraken species
    # Generate by grep -P -o "\[s__[^\|]*(?:\t)" testoutput2 | sort -u | wc -l
    #krakennum = 939890
    #readthreshold = 50.0
    #percthreshold = 1

#Using generated statistics produce graphs in an R enviornment
def graph():
    print(">Creating Graphs from Output CSV files")
    if not os.path.exists(args.results):
        print("Exiting as target results directory %s is non-existant" % args.results)
        return
    with open ("sample.R", 'r') as rIn:
        with open(args.results + "/rout.txt", 'w') as rOut:
            subprocess.call(["R", "--vanilla"], stdin=rIn, stdout=rOut, stderr=rOut, cwd=args.results)
    print("R Script terminated")

#Record resource utilization at a system-wide level
def profile_stats_coarse():
    import psutil
    with open('perf.csv', 'w') as pFile:
        pFile.write("%s,%s,%s,%s,%s,%s,%s,%s\n" % ("time", "cpu_usage", "memory_usage", "memory_percent", "disk_read_time", "disk_write_time", "bytes_sent", "bytes_received"))
        while not finished:
            cpu = sum(psutil.cpu_percent(percpu=True))
            mem = psutil.virtual_memory()
            #hardcoded disk
            dsk = psutil.disk_io_counters(perdisk=True)['sda']
            net = psutil.net_io_counters()
            pFile.write("%s,%f,%d,%f,%d,%d,%d,%d\n" % (time.ctime(), cpu, mem[3], mem[2], dsk[4], dsk[5], net[0], net[1]))
            time.sleep(5)

#Record resource utilization only for this process and any children
def profile_stats_fine():
    import psutil
    forLastSeen = {}
    with open('perf.csv', 'w') as pFile:
        pFile.write("%s,%s,%s,%s,%s,%s\n" % ("time", "cpu_usage", "memory_percent", "disk_read", "disk_write", "connections_open"))
        currentProc = psutil.Process(os.getpid())
        while not finished:
            cpu = currentProc.cpu_percent()
            mem = currentProc.memory_percent()
            dsk = currentProc.io_counters()
            dskRd = dsk[2]
            dskWr = dsk[3]
            conn = len(currentProc.connections())
            for child in currentProc.children(recursive=True):
                try:
                    c_cpu = child.cpu_percent()
                    c_mem = child.memory_percent()
                    dsk = child.io_counters()
                    c_dskRd = dsk[2]
                    c_dskWr = dsk[3]

                    cpu += c_cpu
                    mem += c_mem
                    dskRd += c_dskRd
                    dskWr += c_dskWr
                    forLastSeen[child.pid] = (c_cpu, c_mem, c_dskRd, c_dskWr)
                except psutil.Error as err:
                    if err.pid != None:
                        try:
                            os.kill(err.pid, 0)
                            oldStats = forLastSeen[err.pid]
                            cpu += oldStats[0]
                            mem += oldStats[1]
                            dskRd += oldStats[2]
                            dskWr += oldStats[3]
                        except OSError:
                            pass
            pFile.write("%s,%f,%f,%d,%d,%d\n" % (time.ctime(), cpu, mem, dskRd, dskWr, conn))
            time.sleep(5)

#Calls the various functions based on the input parameters
def main():
    if args.profile:
        global finished, print
        printalias = print
        def time_print(*objs, **kwargs):
            printalias(*((time.ctime(),) + objs), **kwargs)
        print = time_print
        finished = False
        if args.fine:
            prof = threading.Thread(target=profile_stats_fine)
        else:
            prof = threading.Thread(target=profile_stats_coarse)
        #term with main thread
        prof.daemon = True
        print('>Starting profile thread and sleeping for 10s')
        prof.start()
        #sleep for initial perf stats
        time.sleep(10)
    if args.download:
        download_files(args.download)
    print("MATCHING KRAKEN/METAPHLAN DB TO FNA FILES")
    # These files are the relevant database
    match_source_files('metamarkers.txt')
    match_source_files('krakenmarkers.txt')
    if args.generateabd:
        abd_gen()
    if args.skipsim:
        print("Not running GemReads.py due to --skipsim")
    else:
        threaded_sim()
    if args.skipprofile:
        print("Not profiling due to --skipprofile")
    else:
        profile()
    if args.skipcalc:
        print("Not performing calculations or graphing due to --skipcalc")
    else:
        calc()
        graph()
    if args.profile:
        finished = True
        #give time for file close
        print("Going to sleep to allow profiler thread to close file")
        time.sleep(10)
main()