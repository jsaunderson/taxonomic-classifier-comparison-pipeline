# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Installation ###

Clone the repository using the command 'git clone https://jsaunderson@bitbucket.org/jsaunderson/taxonomic-classifier-comparison-pipeline.git'

#### Requirements ####

The following programs are required to run the pipeline:

* Python 2.7.10
    * scipy module
    * psutil module (for resource utilization information only)
* GemSIM
* Kraken
* MetaPhlAn


Additionally for the default graph generation:
 
* R
* R packages:
    * ggplot2
    * grid
    * gridExtra
    * reshape
    * scales

### Running the Pipeline ###

#### Environment Setup

Please ensure 'GemReads.py', 'metaphlan2.py', 'merge_metaphlan_tables.py', 'kraken', 'kraken-mpa-report', and 'R' are accessible from your path.

Additionally paths to the GemSIM error model, bowtie database (for MetaPhlAn), and Kraken database should either be coded directly into the pipeline script as argument defaults or specified using arguments at runtime.

#### Arguments

| Letter Argument | Text Argument | Description                               |
| --------------- | ------------- | ----------------------------------------- |
|              -a | --abundance   | Directory for abundance tables            |
|            -b   | --bowtiedb    | Path to bowtie database (MetaPhlAn)       |
|            -c   | --cores       | Number of cores to use                    |
|            -d   | --download    | File containing links to download         |
|            -g   | --generateabd | Generate abundance tables [number to gen] |
|            -k   | --krakendb    | Path to Kraken database                   |
|            -m   | --model       | Path to GemSIM error model                |
|            -o   | --output      | Directory for simulated FASTQ files       |
|                 | --profile     | Record resource utilization               |
|            -r   | --readdepths  | File of read depths to use                |
|                 | --results     | Directory to store generated graphs       |
|            -s   | --storage     | Directory of/for fna files                |
|                 | --skipcalc    | Skip calculation and graphing             |
|                 | --skipprofile | Skip profiling of FASTQ files             |
|                 | --skipsim     | Skip simulation of FASTQ files            |

#### Sample Run

To test the pipeline, navigate to the folder the repository was cloned to and run the command:

~~~
python full.py -d sample/dl.txt -g -r sample/rd.txt
~~~